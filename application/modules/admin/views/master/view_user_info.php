<section class="card">

	<div class="card-body" id='DivIdToPrint'>
		<div class="invoice">
			<header class="clearfix">
					<div class="row">
						<div class="col-sm-6 text-right mt-3 mb-3">
						<div class="row">
        						<div class="col-md-6">
        						<a class="img-thumbnail lightbox"
								href=".jpg"
								data-plugin-options="{ &quot;type&quot;:&quot;image&quot; }"> <img
								class="img-fluid"
								src="<?=base_url();?>uploads/profile_image/profile_<?=$rider['id'];?>.jpg" style="height: 150px;"> <span
								class="zoom"> <i class="fas fa-search"></i>
							</span>
							</a>
        					<div class="col-md-12">
            					<form  action="<?php echo base_url();?>admin/master/update_profile" enctype='multipart/form-data' method="post">
    							<input type="file" class="btn btn-secondary btn-sm" name="profile" style="width: 108px;" />
    							<input type="hidden" name="id" value="<?php echo $this->uri->segment(2);?>" />
    							<button class="btn btn-success btn-sm">update</button>
    						</form>
        							</div>
        						</div>
							</div>
							
					</div>
					<div class="col-sm-6 text-right mt-3 mb-3">
						<div class="row">
        						<div class="col-md-6">
        						<a class="img-thumbnail lightbox"
								href=".jpg"
								data-plugin-options="{ &quot;type&quot;:&quot;image&quot; }"> <img
								class="img-fluid"
								src="<?=base_url();?>uploads/perm_add_image/perm_add_<?=$rider['id'];?>.jpg" style="height: 150px;"> <span
								class="zoom"> <i class="fas fa-search"></i>
							</span>
							</a>
        					<div class="col-md-12">
            					<form  action="<?php echo base_url();?>admin/master/update_perm_add" enctype='multipart/form-data' method="post">
    							<input type="file" class="btn btn-secondary btn-sm" name="perm_add_img" style="width: 108px;" />
    							<input type="hidden" name="id" value="<?php echo $this->uri->segment(2);?>" />
    							<button class="btn btn-success btn-sm">update</button>
    						</form>
        							</div>
        						</div>
							</div>
							
					</div>
					</div>
					
				</header>
				<p class="h5 mb-1 text-dark font-weight-semibold">User Details:    <br>
				<table class="table table-responsive-md invoice-items">


				<tr class="text-dark">
					<th class="text-dark">Mobile:</th>
					<th class="text-dark"><?php echo $rider['mobile'];?></th>
					
							
                                                    
				</tr>
				<tr class="text-dark">
					<th class="text-dark">Whats App:</th>
					<th class="text-dark"><?php echo $rider['whatsapp_num'];?></th>
				</tr>
				<tr class="text-dark">
					<th class="text-dark">EMAIL Id:</th>
					<th class="text-dark"><?php echo $rider['email'];?></th>
				</tr>
				<tr class="text-dark">
					<th class="text-dark">Permanent Address:</th>
					<th class="text-dark"><?php echo $rider['perm_add'];?></th>
				</tr>
				<tr class="text-dark">
					<th class="text-dark">Educational Qualifications:</th>
					<th class="text-dark"><?php echo $rider['qualification'];?></th>
				</tr>
				<tr class="text-dark">
					<th class="text-dark">Postal:</th>
					<th class="text-dark"><?php echo $rider['zipcode'];?></th>
				</tr>
				<tr class="text-dark">
					<th class="text-dark">State:</th>
					<th class="text-dark"><?php echo $rider['state'];?></th>
				</tr>
				<tr class="text-dark">
					<th class="text-dark">District:</th>
					<th class="text-dark"><?php echo $rider['district_or_city'];?></th>
				</tr>
				<tr class="text-dark">
					<th class="text-dark">Constituency:</th>
					<th class="text-dark"><?php echo $rider['constituency'];?></th>
				</tr>
				<tr class="text-dark">
					<th class="text-dark">Father Name:</th>
					<th class="text-dark"><?php echo $rider['father_or_husband'];?></th>
				</tr>
				<tr class="text-dark">
					<th class="text-dark">Date Of Birth:</th>
					<th class="text-dark"><?php echo $rider['dob'];?></th>
				</tr>
				<tr class="text-dark">
					<th class="text-dark">Gender:</th>
					<th class="text-dark"><?php echo ($rider['gender'] == 1)? 'Female': 'Male';?></th>
				</tr>
				<tr class="text-dark">
					<th class="text-dark">Adhaar Card Number:</th>
					<th class="text-dark"><?php echo $rider['adhaar_num'];?></th>
				</tr>
				<tr class="text-dark">
					<th class="text-dark">Marital Status:</th>
					<th class="text-dark"><?php echo ($rider['marital_status'] == 1)? 'Married': 'Unmarried';?></th>
				</tr>
				<tr class="text-dark">
					<th class="text-dark">Vehicle Reg.No:</th>
					<th class="text-dark"><?php echo $rider['vehicle_reg_num'];?></th>
				</tr>
				<tr class="text-dark">
					<th class="text-dark">Languages:</th>
					<th class="text-dark"><?php echo $rider['lang'];?></th>
				</tr>
				<tr>
					<th class="text-dark">RC</th>
					<th class="text-dark">Driving License</th>
					<th class="text-dark">Pan Card</th>
					
				</tr>
				
					<th class="text-dark">
						<div class="thumbnail-gallery">
							<a class="pop" href="#">
    							<img class="img-fluid" src="<?=base_url();?>uploads/rc_image/rc_<?=$rider['id'];?>.jpg" style="width: 225px !important;height: 225px !important;"/>
							</a>
    						<form  action="<?php echo base_url();?>admin/master/update_rc" enctype='multipart/form-data' method="post">
    							<input type="file" class=""  name="rc"/>
    							<input type="hidden" name="id" value="<?php echo $this->uri->segment(2);?>" /><br/><br/>
    							<button class="btn btn-success">update</button>
    						</form>
						</div>
					</th>
					
					<th class="text-dark">
						<div class="thumbnail-gallery">
							<a class="pop" href="#">
    							<img class="img-fluid" src="<?=base_url();?>uploads/dl_image/dl_<?=$rider['id'];?>.jpg" style="width: 225px !important;height: 225px !important;"/>
							</a>
    						<form  action="<?php echo base_url();?>admin/master/update_dl" enctype='multipart/form-data' method="post">
    							<input type="file" class=""  name="dl"/>
    							<input type="hidden" name="id" value="<?php echo $this->uri->segment(2);?>" /><br/><br/>
    							<button class="btn btn-success">update</button>
    						</form>
						</div>
					</th>
					
					<th class="text-dark">
						<div class="thumbnail-gallery">
							<a class="pop" href="#">
    							<img class="img-fluid" src="<?=base_url();?>uploads/pan_image/pan_<?=$rider['id'];?>.jpg" style="width: 225px !important;height: 225px !important;"/>
							</a>
    						<form  action="<?php echo base_url();?>admin/master/update_pan" enctype='multipart/form-data' method="post">
    							<input type="file" class=""  name="pan"/>
    							<input type="hidden" name="id" value="<?php echo $this->uri->segment(2);?>" /><br/><br/>
    							<button class="btn btn-success">update</button>
    						</form>
						</div>
					</th>
					</tr>
					<tr>
        				<th class="text-dark">Voter Id</th>
        				<th class="text-dark">Vehicle</th>
        				<th class="text-dark">Qualification</th>
        			</tr>
				
					<tr>
					<th class="text-dark">
						<div class="thumbnail-gallery">
							<a class="pop" href="#">
    							<img class="img-fluid" src="<?=base_url();?>uploads/voterid_image/voterid_<?=$rider['id'];?>.jpg" style="width: 225px !important;height: 225px !important;"/>
							</a>
    						<form  action="<?php echo base_url();?>admin/master/update_voterid" enctype='multipart/form-data' method="post">
    							<input type="file" class=""  name="voterid"/>
    							<input type="hidden" name="id" value="<?php echo $this->uri->segment(2);?>" /><br/><br/>
    							<button class="btn btn-success">update</button>
    						</form>
						</div>
					</th>
					<th class="text-dark">
						<div class="thumbnail-gallery">
							<a class="pop" href="#">
    							<img class="img-fluid" src="<?=base_url();?>uploads/vehicle_image/vehicle_<?=$rider['id'];?>.jpg" style="width: 225px !important;height: 225px !important;"/>
							</a>
    						<form  action="<?php echo base_url();?>admin/master/update_vehicle" enctype='multipart/form-data' method="post">
    							<input type="file" class=""  name="vehicle"/>
    							<input type="hidden" name="id" value="<?php echo $this->uri->segment(2);?>" /><br/><br/>
    							<button class="btn btn-success">update</button>
    						</form>
						</div>
					</th>
					<th class="text-dark">
						<div class="thumbnail-gallery">
							<a class="pop" href="#">
    							<img class="img-fluid" src="<?=base_url();?>uploads/quali_image/quali_<?=$rider['id'];?>.jpg" style="width: 225px !important;height: 225px !important;"/>
							</a>
    						<form  action="<?php echo base_url();?>admin/master/update_quali" enctype='multipart/form-data' method="post">
    							<input type="file" class=""  name="quali"/>
    							<input type="hidden" name="id" value="<?php echo $this->uri->segment(2);?>" /><br/><br/>
    							<button class="btn btn-success">update</button>
    						</form>
						</div>
					</th>
					
				</tr>


			</table>
			
					</div>
					</div>
	</section>
	
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">              
      <div class="modal-body">
      	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <img src="" class="imagepreview" style="width: 100%;" >
      </div>
    </div>
  </div>
</div>
