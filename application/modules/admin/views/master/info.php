
<div class="row">
	<div class="col-12">
		<h4>Info</h4>

		<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List </h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Sno</th>
									<th>Full Name</th>
									<th>Father/Husband Name</th>
									<th>District</th>
									<th>Email</th>
									<th>Mobile</th>
									<th>Timings</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
							<?php if(!empty($info)):?>
    							<?php $sno = 1; foreach ($info as $info):?>
    								<tr>
    									<td><?php echo $sno++;?></td>
    									<td><?php echo $info['full_name'];?></td>
    									<td><?php echo $info['father_or_husband'];?></td>
    									<td><?php echo $info['district_or_city'];?></td>
    									<td><?php echo $info['email'];?></td>
    									<td><?php echo $info['mobile'];?></td>
    									<td><?php echo $info['created_at'];?></td>
    									<td>
    									<a href="#" class="mr-2  text-danger " onClick="delete_record(<?php echo $info['id'];?>, 'no_vehicle')"> <i	class="far fa-trash-alt"></i>
    									</a>
    									<a href="<?=base_url();?>/view_user_info/<?php echo $info['id'];?>" target="_blank" class=" mr-2  " type="category" > <i class="fas fa-eye"></i>
    									</a>
    									</td>
    								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr ><th colspan='8'><h3><center>No Vendor</center></h3></th></tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>

	</div>
</div>