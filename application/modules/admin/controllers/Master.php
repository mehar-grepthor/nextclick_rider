<?php

class Master extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->template = 'template/admin/main';
        if (! $this->ion_auth->logged_in())
            redirect('auth/login');
        
        $this->load->model('user_model');
        $this->load->model('setting_model');
        $this->load->model('rider_model');
        $this->load->model('permission_model');
       
    }

    public function view_user_info($id)
    {
        $this->data['title'] = 'User Info';
        $this->data['content'] = 'master/view_user_info';
        $this->data['rider'] = $this->rider_model->where('id', $id)->get();
        //print_array($this->data['rider']);
        $this->_render_page($this->template, $this->data);
    }
    
    public function update_rc()
    {
        $user_id = $this->input->post('id');
        if ($_FILES['rc']['name'] !== '') {
            move_uploaded_file($_FILES['rc']['tmp_name'], "./uploads/rc_image/rc_$user_id.jpg");
        }
        redirect('view_user_info/'.$user_id);
    }
    
    public function update_profile()
    {
        $user_id = $this->input->post('id');
        if ($_FILES['profile']['name'] !== '') {
            move_uploaded_file($_FILES['profile']['tmp_name'], "./uploads/profile_image/profile_$user_id.jpg");
        }
        redirect('view_user_info/'.$user_id);
    }
    
    public function update_dl()
    {
        $user_id = $this->input->post('id');
        if ($_FILES['dl']['name'] !== '') {
            move_uploaded_file($_FILES['dl']['tmp_name'], "./uploads/dl_image/dl_$user_id.jpg");
        }
        redirect('view_user_info/'.$user_id);
    }
    
    public function update_perm_add()
    {
        $user_id = $this->input->post('id');
        if ($_FILES['perm_add_img']['name'] !== '') {
            move_uploaded_file($_FILES['perm_add_img']['tmp_name'], "./uploads/perm_add_image/perm_add_$user_id.jpg");
        }
        redirect('view_user_info/'.$user_id);
    }
    
    public function update_pan()
    {
        $user_id = $this->input->post('id');
        if ($_FILES['pan']['name'] !== '') {
            move_uploaded_file($_FILES['pan']['tmp_name'], "./uploads/pan_image/pan_$user_id.jpg");
        }
        redirect('view_user_info/'.$user_id);
    }
    public function update_voterid()
    {
        $user_id = $this->input->post('id');
        if ($_FILES['voterid']['name'] !== '') {
            move_uploaded_file($_FILES['voterid']['tmp_name'], "./uploads/voterid_image/voterid_$user_id.jpg");
        }
        redirect('view_user_info/'.$user_id);
    }
    public function update_vehicle()
    {
        $user_id = $this->input->post('id');
        if ($_FILES['vehicle']['name'] !== '') {
            move_uploaded_file($_FILES['vehicle']['tmp_name'], "./uploads/vehicle_image/vehicle_$user_id.jpg");
        }
        redirect('view_user_info/'.$user_id);
    }
    public function update_quali()
    {
        $user_id = $this->input->post('id');
        if ($_FILES['quali']['name'] !== '') {
            move_uploaded_file($_FILES['quali']['tmp_name'], "./uploads/quali_image/quali_$user_id.jpg");
        }
        redirect('view_user_info/'.$user_id);
    }
    
    public function constituency($type = 'r')
    {
        /* if (! $this->ion_auth_acl->has_permission('constituency'))
            redirect('admin'); */
        
        if ($type == 'c') {
            $this->form_validation->set_rules($this->constituency_model->rules);
            if ($this->form_validation->run() == FALSE) {
               $this->constituency('r');
            } else {
                $id = $this->constituency_model->insert([
                    'state_id' => $this->input->post('state_id'),
                    'district_id' => $this->input->post('dist_id'),
                    'name' => $this->input->post('name'),
                    'pincode' => $this->input->post('pincode')
                ]);
                redirect('constituency/r', 'refresh');
            }
        } elseif ($type == 'r') {
            $this->data['title'] = 'Constituency';
            $this->data['content'] = 'master/constituency';
            $this->data['states'] = $this->state_model->get_all();
            $this->data['districts'] = $this->district_model->get_all();
            $this->data['constituencies'] = $this->constituency_model->order_by('id', 'DESC')->get_all();
            $this->_render_page($this->template, $this->data);
            //echo json_encode($this->data);
        } elseif ($type == 'u') {
            $this->form_validation->set_rules($this->constituency_model->rules);
            if ($this->form_validation->run() == FALSE) {
                echo validation_errors();
            } else {
                $this->constituency_model->update([
                    'id' => $this->input->post('id'),
                    'state_id' => $this->input->post('state_id'),
                    'district_id' => $this->input->post('dist_id'),
                    'name' => $this->input->post('name'),
                    'pincode' => $this->input->post('pincode')
                ], 'id');
                redirect('constituency/r', 'refresh');
            }
        } elseif ($type == 'd') {
            $this->constituency_model->delete(['id' => $this->input->post('id')]);
        }elseif($type == 'edit'){
            $this->data['title'] = 'Edit Constituency';
            $this->data['content'] = 'master/edit';
            $this->data['type'] = 'constituency';
            $this->data['districts'] = $this->district_model->get_all();
            $this->data['states'] = $this->state_model->get_all();
            $this->data['constituency']= $this->constituency_model->order_by('id', 'DESC')->where('id',$this->input->get('id'))->get();
            $this->_render_page($this->template, $this->data);
        }
    }
   
    /**
     * vendors crud
     *
     * @author Mehar
     * @param string $type
     * @param string $target
     */
    public function vendors($type = 'all')
    {
        /* if (! $this->ion_auth_acl->has_permission('vendor_list'))
            redirect('admin'); */

            if ($type == 'all') {
                $this->data['title'] = 'All Vendors';
                $this->data['content'] = 'master/vendor_list';
                $this->data['type'] = 'all';
                $this->data['categories'] = $this->category_model->get_all();
                $this->data['vendors'] = $this->vendor_list_model->order_by('id', 'DESC')->with_location('fields:id, address')->get_all();
                $this->_render_page($this->template, $this->data);
            } elseif ($type == 'approved') {
                $this->data['title'] = 'Approved Vendors';
                $this->data['content'] = 'master/vendor_list';
                $this->data['type'] = 'approved';
                $this->data['categories'] = $this->category_model->get_all();
                $this->data['vendors'] = $this->vendor_list_model->order_by('id', 'DESC')->with_location('fields:id, address')->where(['status'=> 1])->get_all();
                $this->_render_page($this->template, $this->data);
            } elseif ($type == 'pending') {
                $this->data['title'] = 'Pending Vendors';
                $this->data['content'] = 'master/vendor_list';
                $this->data['type'] = 'pending';
                $this->data['categories'] = $this->category_model->get_all();
                $this->data['vendors'] = $this->vendor_list_model->order_by('id', 'DESC')->with_location('fields:id, address')->where(['status'=> 2])->get_all();
                $this->_render_page($this->template, $this->data);
            } elseif($type == 'vendor'){
                if(!empty($_GET['vendor_id'])){
                $this->data['title'] = 'Vendor Details';
                $this->data['content'] = 'master/vendor_view';
                $this->data['type'] = 'vendor_view';
                $this->data['vendor_list'] = $this->vendor_list_model
                    ->with_location('fields: id, address, latitude, longitude') 
                    ->with_category('fields: id, name')
                    ->with_constituency('fields: id, name, state_id, district_id')
                    ->with_contacts('fields: id, std_code, number, type')
                    ->with_links('fields: id,   url, type')
                    ->with_amenities('fields: id, name')
                    ->with_services('fields: id, name')
                    ->with_holidays('fields: id')
                    ->where('id', $_GET['vendor_id'])->get();

                $this->_render_page($this->template, $this->data);
            }

            }elseif ($type == 'd') {
                $this->vendor_list_model->delete(['id' => $this->input->post('id')]);
            }elseif($type == 'cancelled'){
                $this->data['title'] = 'Cancelled Vendors';
                $this->data['content'] = 'master/vendor_list';
                $this->data['type'] = 'cancelled';
                $this->data['categories'] = $this->category_model->get_all();
                $this->data['vendors'] = $this->vendor_list_model->order_by('id', 'DESC')->with_location('fields:id, address')->only_trashed()->get_all();
                $this->_render_page($this->template, $this->data);
            }elseif($type == 'change_status'){
                /* if(! $this->ion_auth_acl->has_permission('vendor_approval'))
                    redirect('admin/dashboard', 'refresh'); */
                
                 /* $groups = $this->user_model->with_groups('fields: id, priority')->where('id', $this->input->post('user_id'))->get()['groups'];
                 $highest_priority = min(array_column($groups, 'priority'));
                 $group = array();
                 foreach($groups as $a){
                     if($a['priority'] == $highest_priority)
                         $group[]=$a;
                 }
                 if($group[0]['id'] == 1){
                     $approved_by = 3;
                     $status = ($this->input->post('is_checked') == 'true') ? 1 : 2;
                 }elseif ($group[0]['id'] == 2){
                     $approved_by = 2;
                     $status = 2;
                 }elseif ($group[0]['id'] == 3){
                     $approved_by = 1;
                     $status = 2;
                 } */
                     
                 $this->vendor_list_model->update([
                     'status' => ($this->input->post('is_checked') == 'true') ? 1 : 2
                 ], $this->input->post('vendor_id'));
                 $exe = $this->vendor_list_model->with_executive('fields: id, wallet')->where('id', $this->input->post('vendor_id'))->as_array()->get();
                  $this->user_model->update([
                     'id' => $exe['executive']['id'],
                      'wallet' => ($this->input->post('is_checked') == 'true') ? $exe['executive']['wallet'] + floatval($this->setting_model->where('key','pay_per_vendor')->get()['value']) : $exe['executive']['wallet']
                 ], 'id');
                 if($_POST['is_checked'] == 'true'){
                      $id = $this->wallet_transaction_model->insert([
                          'user_id' => $exe['executive']['id'],
                          'type' => 'CREDIT',
                          'cash' => floatval($this->setting_model->where('key','pay_per_vendor')->get()['value']),
                          'description' => $exe['name'],
                          'status' => 1
                      ]);
                      echo json_encode($exe);
                 }
            }elseif ($type == 'cover_update'){
                $user_id = $this->input->post('id');
                if ($_FILES['cover']['name'] !== '') {
                    move_uploaded_file($_FILES['cover']['tmp_name'], "./uploads/list_cover_image/list_cover_$user_id.jpg");
                }
                redirect('vendors/vendor?vendor_id='.$user_id);
            }
    }
    public function no_vehicle($type = 'r')
    {
        if($type == 'r') {
        $this->data['title'] = 'Info';
        $this->data['content'] = 'master/info';
        $this->data['info'] = $this->rider_model->where(['own_vehicle'=>'0'])->order_by('id','DESC')->get_all();
        $this->_render_page($this->template, $this->data);
        }elseif ($type == 'd'){
            $this->rider_model->delete(['id' => $this->input->post('id')]);
        }
    }
    public function own_vehicle($type = 'r')
    {
        if($type == 'r'){
        $this->data['title'] = 'Info';
        $this->data['content'] = 'master/info_own';
        $this->data['info'] = $this->rider_model->where(['own_vehicle'=>'1'])->order_by('id','DESC')->get_all();
        $this->_render_page($this->template, $this->data);
        }elseif ($type =='d'){
            $this->rider_model->delete(['id' => $this->input->post('id')]);
        }
    }
    
}

