<?php
require APPPATH . '/libraries/MY_REST_Controller.php';
require APPPATH . '/vendor/autoload.php';

use Firebase\JWT\JWT;

class Master extends MY_REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('category_model');
        $this->load->model('sub_category_model');
        $this->load->model('amenity_model');
        $this->load->model('service_model');
        $this->load->model('state_model');
        $this->load->model('district_model');
        $this->load->model('constituency_model');
        $this->load->model('day_model');
        $this->load->model('rating_model');
    }
    
    /**
     * @author Mehar
     * @desc To get states and relatd details
     * @param string $target
     * @param string $district_id
     * @param string $constituency_id
     */
    public function states_get($target = '', $district_id = '', $constituency_id = '') {
        //$this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        if(empty($target)){
            $data = $this->state_model->get_all();
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE); 
        }elseif (! empty($target) && empty($district_id)){
            $data = $this->state_model->with_districts('fields:name,id')->where('id', $target)->get();
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }elseif (! empty($target) && ! empty($district_id) && empty($constituency_id)){
            $data = $this->district_model->with_constituenceis('fields:name, id')->where('id', $district_id)->get();
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }else{
            $data = $this->constituency_model->where('id', $constituency_id)->get();
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }
    }
    
    /**
     * @author Mehar
     * @desc To get list of categories and targeted category as well
     * @param string $target
     */
    public function categories_get($target = '') {
        //$this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        if(empty($target)){
            $data = $this->category_model->order_by('name', 'ASC')->get_all();
            if(! empty($data)){
                for ($i = 0; $i < count($data) ; $i++){
                    $data[$i]['image'] = base_url().'uploads/category_image/category_'.$data[$i]['id'].'.jpg';
                }
            }
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }else{
            $data = $this->category_model->with_amenities('fields: name, id')->with_sub_categories('fields: name, id')->with_services('fields: name, id')->where('id', $target)->get();
            $data['image'] = base_url().'uploads/category_image/category_'.$data['id'].'.jpg';
            if(! empty($data['sub_categories'])){
                for ($i = 0; $i < count($data['sub_categories']) ; $i++){
                    $data['sub_categories'][$i]['image'] = base_url().'uploads/sub_category_image/sub_category_'.$data['sub_categories'][$i]['id'].'.jpg';
                }
            }
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }
    }
    
    /**
     * @author Mehar
     * @desc To get list of sub categories 
     * @param string $target
     */
    public function sub_categories_get($target = '') {
        //$this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        if(empty($target)){
            $data = $this->sub_category_model->get_all();
            if(! empty($data)){
                for ($i = 0; $i < count($data) ; $i++){
                    $data[$i]['image'] = base_url().'uploads/sub_category_image/sub_category_'.$data[$i]['id'].'.jpg';
                }
            }
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }else{
            $data = $this->sub_category_model->where('id', $target)->get();
            $data['image'] = base_url().'uploads/sub_category_image/sub_category_'.$data['id'].'.jpg';
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }
    }
    
    /**
     * @author Mehar
     * @desc To get list of amenities and targeted amenity as well
     * @param string $target
     */
    public function amenities_get($target = '') {
        $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        if(empty($target)){
            $data = $this->amenity_model->get_all();
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }else{
            $data = $this->amenity_model->where('id', $target)->get();
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }
    }
    
    /**
     * @author Mehar
     * @desc To get list of services and targeted service as well
     * @param string $target
     */
    public function services_get($target = '') {
        $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        if(empty($target)){
            $data = $this->service_model->get_all();
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }else{
            $data = $this->service_model->where('id', $target)->get();
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }
    }
    
    /**
     * @author Mehar
     * @desc To get list of days and day service as well
     * @param string $target
     */
    public function days_get($target = '') {
        $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        if(empty($target)){
            $data = $this->day_model->get_all();
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }else{
            $data = $this->day_model->where('id', $target)->get();
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }
    }
    /**
     * @author Mahesh
     * @desc To get the Sliders Details
     *
     */
 public function sliders_get()
    {
            $this->load->model('sliders_model');
        $this->load->model('advertisements_model');
    /*
        $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
*/
            $sliders = $this->sliders_model->get_all();
            $top = $this->advertisements_model->where('type','top')->get_all();
            $middle = $this->advertisements_model->where('type','middle')->get_all();
            $bottom = $this->advertisements_model->where('type','bottom')->get_all();
            if(! empty($sliders)){
               for ($i = 0; $i < count($sliders) ; $i++){
                   $data1[$i]['image'] = base_url().'uploads/sliders_image/sliders_'.$sliders[$i]['id'].'.'.$sliders[$i]['ext'];
               }
               $res['sliders']=$data1;
           }
           if(! empty($top)){
               for ($i = 0; $i < count($top) ; $i++){
                   $data2[$i]['image'] = base_url().'uploads/advertisements_image/advertisements_'.$top[$i]['id'].'.'.$top[$i]['ext'];
               }
               $res['top']=$data2;
           }
           if(! empty($middle)){
               for ($i = 0; $i < count($middle) ; $i++){
                   $data3[$i]['image'] = base_url().'uploads/advertisements_image/advertisements_'.$middle[$i]['id'].'.'.$middle[$i]['ext'];
               }
               $res['middle']=$data3;
           }
           if(! empty($bottom)){
               for ($i = 0; $i < count($bottom) ; $i++){
                   $data4[$i]['image'] = base_url().'uploads/advertisements_image/advertisements_'.$bottom[$i]['id'].'.'.$bottom[$i]['ext'];
               }
               $res['bottom']=$data4;
           }
      $this->set_response_simple(($res == FALSE)? FALSE : $res, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    }
    
    
    /**
     * @desc To Manage reviews
     * @author Mehar
     * 
     * @param string $type
     */
    public function ratings_post($type = 'r'){
        $_POST = json_decode(file_get_contents("php://input"), TRUE);
        if($type == 'r'){
            $data = $this->rating_model->order_by('id', 'DESC')->fields('id, rating, review, created_at')->with_user('fields: id, first_name, last_name')->where('vendor_id', $this->input->post('vendor_id'))->get_all();
            if(! empty($data)){
                for ($i = 0; $i < count($data) ; $i++){
                    $data[$i]['user']['image'] = base_url().'uploads/profile_image/profile_'.$data[$i]['user_id'].'.jpg';
                }
            }
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }elseif ($type == 'c'){
            $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
            $is_exist = $this->rating_model->where(['user_id' => $token_data->id, 'vendor_id' => $this->input->post('vendor_id')])->get();
            if(! empty($is_exist)){
                $id = $this->rating_model->delete(['id' => $is_exist['id']]);
            }
            $id = $this->rating_model->insert([
                'user_id' => $token_data->id,
                'vendor_id' => $this->input->post('vendor_id'),
                'rating' => $this->input->post('rating'),
                'review' => $this->input->post('review')
            ]);
            $this->set_response_simple($id, 'Success..!', REST_Controller::HTTP_CREATED, TRUE);
        }
    }
}

