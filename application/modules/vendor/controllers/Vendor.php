<?php
class Vendor extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->template = 'template/admin/main';
        if (! $this->ion_auth->logged_in()) // || ! $this->ion_auth->is_admin()
            redirect('auth/login');
           
            $this->load->model('vendor_bank_details_model');
            $this->load->model('vendor_list_model');
    }

    /**
     * Vendor Profile Settings
     *
     * @author Mahesh
     * @desc To Manage Vendor Details
     * @param string $type
     * @param string $target
     */
    public function vendor_profile($type = 'r',$u_type='')
    {
        
        /*if (! $this->ion_auth_acl->has_permission('vendor'))
            redirect('admin');*/

            if ($type == 'r') {
                $this->data['title'] = 'Vendor Profile';
                $this->data['content'] = 'vendor/vendor/vendor_profile';
                $this->data['bank_details'] = $this->vendor_bank_details_model->fields('id,bank_name,bank_branch,ifsc,ac_holder_name,ac_number')->where('vendor_id', $this->ion_auth->get_user_id())->get();
                $this->data['vendor_details'] = $this->vendor_list_model
        ->with_location('fields: id, address, latitude, longitude') 
        ->with_category('fields: id, name')
        ->with_constituency('fields: id, name, state_id, district_id')
        ->with_contacts('fields: id, std_code, number, type')
        ->with_links('fields: id,   url, type')
        ->with_amenities('fields: id, name')
        ->with_services('fields: id, name')
        ->with_holidays('fields: id')
        ->where('id', $this->ion_auth->get_user_id())->get();
                $this->_render_page($this->template, $this->data);
            } elseif ($type == 'u') {
                if($u_type=='bank_details'){
                    $this->form_validation->set_rules($this->vendor_bank_details_model->rules);
                if ($this->form_validation->run() == FALSE) {
                    echo validation_errors();
                } else {
                    $r=$this->vendor_bank_details_model->fields('id')->where('vendor_id',$this->ion_auth->get_user_id())->get();
                    if($r!=''){
                    $this->vendor_bank_details_model->update([
                        'bank_name' => $this->input->post('bank_name'),
                        'bank_branch' => $this->input->post('bank_branch'),
                        'ifsc' => $this->input->post('ifsc'),
                        'ac_holder_name' => $this->input->post('ac_holder_name'),
                        'ac_number' => $this->input->post('ac_number')
                    ], ['vendor_id',$this->ion_auth->get_user_id()]);
                    redirect('vendor_profile/r', 'refresh');
                }else{
                    $this->vendor_bank_details_model->insert([
                        'bank_name' => $this->input->post('bank_name'),
                        'bank_branch' => $this->input->post('bank_branch'),
                        'ifsc' => $this->input->post('ifsc'),
                        'ac_holder_name' => $this->input->post('ac_holder_name'),
                        'ac_number' => $this->input->post('ac_number'),
                        'vendor_id'=>$this->ion_auth->get_user_id()
                    ]);
                    redirect('vendor_profile/r', 'refresh');
                }
                }
                }
                
            }
    }
}