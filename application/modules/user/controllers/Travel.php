<?php
require APPPATH . '/libraries/MY_REST_Controller.php';
require APPPATH . '/vendor/autoload.php';
use Firebase\JWT\JWT;
class Travel extends MY_REST_Controller
{
   public function __construct()
   {
       parent::__construct();
       	$this->load->model('travel_brand_model');
		$this->load->model('travel_accessorie_model');
		$this->load->model('travel_vehicle_model');
		$this->load->model('travel_veh_acc_model');
   }
   /**
    * @author Mahesh
    * @desc To get list of Brands
    * @param string $target
    */
   public function TravelBrands_get() {
       
           $data = $this->travel_brand_model->fields('id,name')->get_all();
           $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
       
   }
   /**
    * @author Mahesh
    * @desc To get list of Accessories
    * @param string $target
    */
   public function TravelAccessories_get() {
       
           $data = $this->travel_accessorie_model->fields('id,name')->get_all();
           $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
         
   }
   /**
    * @author Mahesh
    * @desc To get list of Vehicles
    * @param string $target
    */
   public function Vehicles_get() {
       if(! empty($this->input->get('vendor_id')))
       {
       	$vendor_id=$this->input->get('vendor_id');
           $data = $this->travel_vehicle_model->fields('id,name,brand_id,model_year,desc,no_of_vehicles,price_per_day,price_per_km,seating,fuel')->with_accessories('fields:id,name')->with_brands('fields:id,name')->where('vendor_id', $vendor_id)->get_all();
           $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }
        if(! empty($this->input->get('vehicle_id')))
       {
       	$vehicle_id=$this->input->get('vehicle_id');
           $data = $this->travel_vehicle_model->fields('id,name,brand_id,model_year,desc,no_of_vehicles,price_per_day,price_per_km,seating,fuel')->with_accessories('fields:id,name')->with_brands('fields:id,name')->where('id', $vehicle_id)->get();
           $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }
   }
}
