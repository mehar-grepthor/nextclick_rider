<?php
require APPPATH . '/libraries/MY_REST_Controller.php';
require APPPATH . '/vendor/autoload.php';
use Firebase\JWT\JWT;
class Home_service extends MY_REST_Controller
{
   public function __construct()
   {
      parent::__construct();
      $this->load->model('home_service_type_model');
		  $this->load->model('home_service_users_model');
      $this->load->model('home_service_order_model');
      $this->load->model('home_service_ord_ser_model');
   }
   /**
    * @author Mahesh
    * @desc To get list of Home Service Type
    * @param string $target
    */
   public function HomeServiceType_get() {
           $data = $this->home_service_type_model->fields('id,name')->get_all();
           $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
   }
   /**
    * @author Mahesh
    * @desc To get list of Home Service Users
    * @param string $target
    */
   public function HomeServiceUsers_get() {
       if(! empty($this->input->get('vendor_id')))
       {
       	$vendor_id=$this->input->get('vendor_id');
           $data = $this->home_service_users_model->fields('id,name,mobile,email')->with_service_type('fields:id,name')->where('vendor_id', $vendor_id)->get_all();
           if(! empty($data)){
               for ($i = 0; $i < count($data) ; $i++){
                  $data[$i]['service_type']=$data[$i]['service_type']['name'];
                   $data[$i]['image'] = base_url().'uploads/home_service_users_image/home_service_users_'.$data[$i]['id'].'.jpg';
               }
           }
           $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }
        if(! empty($this->input->get('user_id')))
       {
       	$user_id=$this->input->get('user_id');
           $data = $this->home_service_users_model->fields('id,name,mobile,email,bank_name,ifsc,ac_holder,ac_number,google_pay,phone_pay,paytm')->with_service_type('fields:id,name')->where('id', $user_id)->get();
           if(! empty($data)){
                   $data['image'] = base_url().'uploads/home_service_users_image/home_service_users_'.$data['id'].'.jpg';
           }
           $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }
   }
   /**
    * @author Mahesh
    * @desc To POST data for booking a home service
    * @param string $target
    */
   public function HomeServices_POST()
   {
      $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
      $_POST = json_decode(file_get_contents("php://input"), TRUE);
            $this->form_validation->set_rules($this->home_service_order_model->rules);
            if ($this->form_validation->run() == false) {
                $this->set_response_simple(validation_errors(), 'Validation Error', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
            } else {
              $order_id = $this->home_service_order_model->insert([
                    'user_id' => $token_data->id,
                    'order_track' => rand(),
                    'date' => $this->input->post('date'),
                    'time' => $this->input->post('time'),
                    'address_id' => $this->input->post('address_id'),
                    'instructions' => $this->input->post('instructions'),
                    'vendor_id' => $this->input->post('vendor_id'),
                ]);
                if(!empty($order_id)){
                    foreach ($_POST['services'] as $item){
                        $item_id = $this->home_service_ord_ser_model->insert([
                            'order_id' => $order_id,
                            'service_id' => $item['service_id']
                        ]);
                    }
                  }
              $this->set_response_simple($order_id, 'Success..!', REST_Controller::HTTP_CREATED, TRUE);
            }
   }
}
