<?php
require APPPATH . '/libraries/MY_REST_Controller.php';
require APPPATH . '/vendor/autoload.php';
use Firebase\JWT\JWT;
class Grocery extends MY_REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('grocery_category_model');
        $this->load->model('grocery_sub_category_model');
        $this->load->model('grocery_brand_model');
        $this->load->model('grocery_product_model');
        //$this->load->model('grocery_cart_model');
        $this->load->model('users_address_model');
        $this->load->model('grocery_order_model');
        $this->load->model('grocery_order_details_model');
    }
    /**
     * @author Mahesh
     * @desc To get list of categories and targeted category as well
     * @param string $target
     */
    public function GroceryCategories_get($target = '') {
        if(empty($target)){
            $data = $this->grocery_category_model->fields('id, name, desc')->get_all();
            if(! empty($data)){
                for ($i = 0; $i < count($data) ; $i++){
                    $data[$i]['image'] = base_url().'uploads/grocery_category_image/grocery_category_'.$data[$i]['id'].'.png';
                }
            }
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }else{
            $data = $this->grocery_category_model->fields('id, name, desc')->with_grocery_sub_categories('fields: name, id')->where('id', $target)->get();
            $data['image'] = base_url().'uploads/grocery_category_image/grocery_category_'.$target.'.png';
            if(! empty($data['grocery_sub_categories'])){
                        for ($i = 0; $i < count($data['grocery_sub_categories']) ; $i++){
                            $data['grocery_sub_categories'][$i]['image'] = base_url().'uploads/grocery_sub_category_image/grocery_sub_category_'.$data['grocery_sub_categories'][$i]['id'].'.jpg';
                        }
                    }
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }
    }
    /**
     * @author Mahesh
     * @desc To get list of sub categories and targeted category as well
     * @param string $target
     */
    public function GrocerySubCategories_get($target = '') {
        if(empty($target)){
            $data = $this->grocery_sub_category_model->fields('id, name, desc')->get_all();
            if(! empty($data)){
                for ($i = 0; $i < count($data) ; $i++){
                    $data[$i]['image'] = base_url().'uploads/grocery_sub_category_image/grocery_sub_category_'.$data[$i]['id'].'.jpg';
                }
            }
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }else{
            $data = $this->grocery_sub_category_model->fields('id, name, desc')->with_brands('fields: name, id')->where('id', $target)->get();
            if(!empty($data)){
            $data['image'] = base_url().'uploads/grocery_sub_category_image/grocery_sub_category_'.$target.'.jpg';
            }
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }
    }
    
    /**
     * @author Mahesh
     * @desc To get list of brands
     * @param string $target
     */
    public function GroceryBrands_get($target = '') {
        if(empty($target)){
            $data = $this->grocery_brand_model->fields('id, name, desc')->get_all();
            if(! empty($data)){
                for ($i = 0; $i < count($data) ; $i++){
                    $data[$i]['image'] = base_url().'uploads/grocery_brands_image/grocery_brands_'.$data[$i]['id'].'.jpg';
                }
            }
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }else{
            $data = $this->grocery_brand_model->fields('id, name, desc')->where('id', $target)->get_all();
            if(!empty($data)){
            $data['image'] = base_url().'uploads/grocery_brands_image/grocery_brands_'.$target.'.jpg';
            }
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }
    }
    
    /**
     * @author Mahesh
     * @desc To get list of brands
     * @param string $limit
     * @param string $offset
     */
    public function GroceryProducts_get($limit = 10, $offset = 0){
        if(! isset($_GET['product_id'])){
            $data = $this->grocery_product_model->all($limit, $offset, $this->input->get('sub_cat_id'), $this->input->get('q'));
            if(! empty($data['result'])){
                foreach  ($data['result'] as $product){
                    $product->image = base_url().'uploads/grocery_product_image/grocery_product_'.$product->id.'.jpg';
                }
            }
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }else{
            $data = $this->grocery_product_model
            ->fields('id, name, units, desc, qty, mrp, offer_price, gst')
            ->with_vendor('fields:id, first_name, last_name, unique_id')
            ->with_category('fields:id, name')
            ->with_sub_category('fields:id, name')
            ->with_brand('fields:id, name')->where('id', $this->input->get('product_id'))->get();
            $data['image'] = base_url().'uploads/grocery_product_image/grocery_product_'. $this->input->get('product_id').'.jpg';
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }
    }
    
    /**
     * Grocery Cart
     *
     * @author Mehar
     * @desc To Manage Ecommerce Cart
     * @param string $type
     */
    public function ecom_cart_post($type = 'r'){
        $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
            if ($type == 'c') {
                $_POST = json_decode(file_get_contents("php://input"), TRUE);
                $this->form_validation->set_rules($this->ecom_cart_model->rules);
                if ($this->form_validation->run() == false) {
                    $this->set_response_simple(validation_errors(), 'Validation Error', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
                } else {
                    $id = $this->ecom_cart_model->insert([
                        'user_id' => $token_data->id,
                        'product_id' => $this->input->post('product_id'),
                        'qty' => $this->input->post('qty'),
                    ]);
                    $this->set_response_simple($id, 'Success..!', REST_Controller::HTTP_CREATED, TRUE);
                }
            } elseif ($type == 'r') {
                $data = $this->ecom_cart_model->fields('id, user_id, product_id, qty')->with_product('fields: id, name, units, desc, qty, mrp, offer_price, gst')->get_all('user_id', $token_data->id);
                $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
            }  elseif ($type == 'u') {
                $_POST = json_decode(file_get_contents("php://input"), TRUE);
                $this->form_validation->set_rules($this->ecom_cart_model->rules);
                if ($this->form_validation->run() == FALSE) {
                    $this->set_response_simple(validation_errors(), 'Validation Error', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
                } else {
                    $this->ecom_cart_model->update([
                        'id' => $this->input->post('id'),
                        'user_id' => $token_data->id,
                        'product_id' => $this->input->post('product_id'),
                        'qty' => $this->input->post('qty'),
                    ], 'id');
                    $this->set_response_simple(NULL, 'Success..!', REST_Controller::HTTP_ACCEPTED, TRUE);
                }
            }elseif ($type == 'd') {
                $_POST = json_decode(file_get_contents("php://input"), TRUE);
                $this->ecom_cart_model->delete(['id' => $this->input->post('id')]);
                $this->set_response_simple(NULL, 'Deleted..!', REST_Controller::HTTP_OK, TRUE);
            }
    }
    
    /**
     * Grocery Checkout
     *
     * @author Mahesh
     * @desc To Manage Grocery Check out
     * @param string $type
     */
    public function GroceryCheckout_post(){
            $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
            $_POST = json_decode(file_get_contents("php://input"), TRUE);
            $this->form_validation->set_rules($this->users_address_model->rules);
            $this->form_validation->set_rules($this->grocery_order_model->rules);
            if ($this->form_validation->run() == false) {
                $this->set_response_simple(validation_errors(), 'Validation Error', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
            } else {

                $address_id = '';
                if(empty($_POST['address_id'])){
                    $address_id = $this->users_address_model->insert([
                        'user_id' => $token_data->id,
                        'name' => $this->input->post('name'),
                        'email' => $this->input->post('email'),
                        'phone' => $this->input->post('mobile'),
                        'address' => $this->input->post('address'),
                    ]);
                }else {
                    $address_id = $_POST['address_id'];
                }

                $order_id = $this->grocery_order_model->insert([
                    'user_id' => $token_data->id,
                    'order_no' => rand(),
                    'discount' => $this->input->post('discount'),
                    'tax' => $this->input->post('tax'),
                    'total' => $this->input->post('total'),
                    'coupon_id' => $this->input->post('coupon_id'),
                    'address_id' => $address_id,
                    'payment_method_id' => $this->input->post('payment_method_id')
                ]);
                if(!empty($order_id)){
                    foreach ($_POST['products'] as $product){
                        $order_id = $this->grocery_order_details_model->insert([
                            'order_id' => $order_id,
                            'product_id' => $product['product_id'],
                            'qty' => $product['qty'],
                            'price' => $product['price'],
                        ]);
                    }
                }
                $this->set_response_simple($order_id, 'Success..!', REST_Controller::HTTP_CREATED, TRUE);
            }
    }
}
