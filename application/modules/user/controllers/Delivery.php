
<?php
require APPPATH . '/libraries/MY_REST_Controller.php';
require APPPATH . '/vendor/autoload.php';
use Firebase\JWT\JWT;
class Delivery extends MY_REST_Controller
{
   public function __construct()
   {
       parent::__construct();
       $this->load->model('delivery_boy_status_model');
       $this->load->model('users_address_model');
       $this->load->model('vendor_list_model');
   }
   /*Delivery Boy App Start */
     /**
    * @author Mahesh
    * @desc To Update Delivery Boy Status
    */
   public function DeliveryBoyStatus_POST($target='r')
   {
  $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
  $v=$this->delivery_boy_status_model->where('user_id',$token_data->id)->get();
  if($target=='u'){
        $input=$this->post();
        
                    if($v != ''){
                    $id_deal=$this->delivery_boy_status_model->update([
                        'delivery_boy_status' => $this->input->post('delivery_boy_status')
                    ], ['user_id',$token_data->id]);
                }else{
                    $id_deal=$this->delivery_boy_status_model->insert([
                        'delivery_boy_status' => $this->input->post('delivery_boy_status'),
                        'user_id'=>$token_data->id
                    ]);
                }
      }elseif($target=='r'){
          $id_deal['status']=$v['delivery_boy_status'];
      }

        $this->set_response_simple(($id_deal == FALSE)? FALSE : $id_deal, 'Success..!', REST_Controller::HTTP_OK, TRUE);
   }
   public function DeliveryBoyLatLong_POST()
   {
  $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        $input=$this->post();
        $v=$this->delivery_boy_status_model->where('user_id',$token_data->id)->get();
                if($v != ''){
                    $id_deal=$this->delivery_boy_status_model->update([
                        'latitude' => $this->input->post('latitude'),
                        'longitude' => $this->input->post('longitude')
                    ], ['user_id',$token_data->id]);
                }else{
                    $id_deal=$this->delivery_boy_status_model->insert([
                        'latitude' => $this->input->post('latitude'),
                        'longitude' => $this->input->post('longitude'),
                        'user_id'=>$token_data->id
                    ]);
                }
        $this->set_response_simple(($id_deal == FALSE)? FALSE : $id_deal, 'Success..!', REST_Controller::HTTP_OK, TRUE);
   }
    /*Delivery Boy App End */


}