<?php
require APPPATH . '/libraries/MY_REST_Controller.php';
require APPPATH . '/vendor/autoload.php';

use Firebase\JWT\JWT;

class Executive extends MY_REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('rider_model');
    }

    public function rider_post($method = 'r')
    {
        if ($method == 'c') {
            $_POST = json_decode(file_get_contents("php://input"), TRUE);
            $this->form_validation->set_rules($this->rider_model->rules);
            if ($this->form_validation->run() == FALSE) {
                $this->set_response_simple(validation_errors(), 'Internal Error Occured..!', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
            } else {
                    
                $id = $this->rider_model->insert([
                    'own_vehicle' => $this->input->post('own_vehicle'),
                    'full_name' => $this->input->post('full_name'),
                    'father_or_husband' => $this->input->post('father_or_husband'),
                    'perm_add' => $this->input->post('perm_add'),
                    'current_add' => $this->input->post('current_add'),
                    'zipcode' => $this->input->post('zipcode'),
                    'state' =>$this->input->post('state'),
                    'district_or_city' => $this->input->post('district_or_city'),
                    'constituency' => $this->input->post('constituency'),
                    'dob' => $this->input->post('dob'),
                    'gender' => $this->input->post('gender'),
                    'lang' => $this->input->post('lang'),
                    'marital_status' => $this->input->post('marital_status'),
                    'qualification' => $this->input->post('qualification'),
                    'email' => $this->input->post('email'),
                    'mobile' => $this->input->post('mobile'),
                    'mobile_1' => $this->input->post('mobile_1'),
                    'whatsapp_num' => $this->input->post('whatsapp_num'),
                    'vehicle_reg_date' => $this->input->post('vehicle_reg_date'),
                    'vehicle_reg_num' => $this->input->post('vehicle_reg_num'),
                    'adhaar_num' => $this->input->post('adhaar_num'),
                    'driving_licen_no' => $this->input->post('driving_licen_no'),
                    'driving_issue_date' => $this->input->post('driving_issue_date'),
                    'license_exp_date' => $this->input->post('license_exp_date'),
                    'pollution_exp_date' => $this->input->post('pollution_exp_date'),
                    'insurance_num' => $this->input->post('insurance_num'),
                    'insurance_date' => $this->input->post('insurance_date'),
                    'insurance_exp_date' => $this->input->post('insurance_exp_date'),
                    'status' => 2
                ]);
                
               /* if (!file_exists('uploads/perm_add_image/')) {
                    mkdir('uploads/perm_add_image/', 0777, true);
                }*/
                file_put_contents("./uploads/perm_add_image/perm_add_$id.jpg", base64_decode($this->input->post('perm_add_img')));
                if (!file_exists('uploads/perm_add_image/')) {
                    mkdir('uploads/perm_add_image/', 0777, true);
                }
                file_put_contents("./uploads/rc_image/rc_$id.jpg", base64_decode($this->input->post('rc')));
                if (!file_exists('uploads/rc_image/')) {
                    mkdir('uploads/rc_image/', 0777, true);
                }
                file_put_contents("./uploads/dl_image/dl_$id.jpg", base64_decode($this->input->post('dl')));
                if (!file_exists('uploads/dl_image/')) {
                    mkdir('uploads/dl_image/', 0777, true);
                }
                file_put_contents("./uploads/pan_image/pan_$id.jpg", base64_decode($this->input->post('pan')));
                if (!file_exists('uploads/pan_image/')) {
                    mkdir('uploads/pan_image/', 0777, true);
                }
                file_put_contents("./uploads/voterid_image/voterid_$id.jpg", base64_decode($this->input->post('voterid')));
                if (!file_exists('uploads/voterid_image/')) {
                    mkdir('uploads/voterid_image/', 0777, true);
                }
                file_put_contents("./uploads/vehicle_image/vehicle_$id.jpg", base64_decode($this->input->post('vehicle')));
                if (!file_exists('uploads/vehicle_image/')) {
                    mkdir('uploads/vehicle_image/', 0777, true);
                }
                file_put_contents("./uploads/quali_image/quali_$id.jpg", base64_decode($this->input->post('quali')));
                if (!file_exists('uploads/quali_image/')) {
                    mkdir('uploads/quali_image/', 0777, true);
                }
                file_put_contents("./uploads/profile_image/profile_$id.jpg", base64_decode($this->input->post('profile')));
                if (!file_exists('uploads/profile_image/')) {
                    mkdir('uploads/profile_image/', 0777, true);
                }
                $this->set_response_simple($id, 'Success..!', REST_Controller::HTTP_CREATED, TRUE);
            }
        }
    }
}

